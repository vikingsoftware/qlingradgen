/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see
   <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#include "gradientgenerator.h"

#include <QDebug>
#include <QFile>
#include <QImage>
#include <QLineF>
#include <QPainterPath>
#include <QtMath>

/*!
 * \brief GradientGenerator::generateCode generates the C++ code based on
 * params from \param config's in.
 * If \param config.in.image is invalid, tries to load the image from \params config.in.path.
 * The \param config.in.direction is a line in image coordinates, used to get pixel values
 * in range [direction.p1(); direction.p2()] with step \param config.in.step.
 * \return true if the generation process succeeds.
 * The \param config.out contains the result of generation, both as string of C++ code
 * and the instance of QLinearGradient.
 */
/*static*/
bool GradientGenerator::generateCode(LinGradGen &config)
{
    if (config.in.image.isNull()) {
        if (config.in.path.isEmpty())
            return fillResult(config, GeneratorResult::Error::NoInputFile,
                              QStringLiteral("The specificied file name is empty."));

        if (!QFile::exists(config.in.path))
            return fillResult(config, GeneratorResult::Error::NoInputFile,
                              QStringLiteral("The specificied file does not exist."));

        if (!config.in.image.load(config.in.path))
            return fillResult(config, GeneratorResult::Error::InvalidImage,
                              QStringLiteral("The specificied image can not be loaded."));
    }

    if (config.in.image.isNull())
        return fillResult(config, GeneratorResult::Error::InvalidImage,
                          QStringLiteral("The specificied image is null."));

    if (!config.in.direction.dx() && !config.in.direction.dy())
        return fillResult(config, GeneratorResult::Error::InvalidDirection,
                          QStringLiteral("The specificied direction is empty."));

    if (!config.in.step)
        return fillResult(config, GeneratorResult::Error::InvalidStep, QStringLiteral("The step can not be 0."));

    const qreal maxD = qMax(qAbs(config.in.direction.dx()), qAbs(config.in.direction.dy()));
    if (config.in.step - 1 > maxD)
        return fillResult(config, GeneratorResult::Error::InvalidStep,
                          QStringLiteral("The specificed step exceeds the target direction."));

    const QRectF &imgRect = config.in.image.rect();
    if (!imgRect.contains(config.in.direction.p1()) || !imgRect.contains(config.in.direction.p2()))
        return fillResult(config, GeneratorResult::Error::InvalidDirection,
                          QStringLiteral("The specificied direction exceeds the image size."));

    if (config.in.varName.isEmpty())
        return fillResult(config, GeneratorResult::Error::NoOutputName,
                          QStringLiteral("The output gradient name can not be empty."));

    return generate(config);
}

/*!
 * \brief GradientGenerator::fillResult - a helper method used to fill the result handler.
 */
/*static*/ bool GradientGenerator::fillResult(LinGradGen &param, GeneratorResult::Error code,
                                              const QString &description)
{
    param.out.status = code;
    param.out.statusDescription = description;
    return !param.out.failed();
}

QString prepareText(const QString &varName, const QPointF &startLogical, const QPointF &endLogical,
                    const QString &stopsText)
{
    static const QString cppText("QLinearGradient get%1()\n"
                                 "{\n"
                                 "    QLinearGradient %2;\n"
                                 "    %2.setStart( %3 );\n"
                                 "    %2.setFinalStop( %4 );\n"
                                 "    %2.setCoordinateMode(QGradient::ObjectMode);\n"
                                 "    const QGradientStops stops = {\n"
                                 "%5"
                                 "    };\n"
                                 "    %2.setStops(stops);\n"
                                 "    return %2;\n"
                                 "}\n");

    auto pointToString = [](const QPointF &point) {
        return QStringLiteral("%1, %2").arg(QString::number(point.x()), QString::number(point.y()));
    };

    QString funcName(varName);
    funcName.replace(0, 1, funcName.at(0).toUpper());

    return cppText.arg(funcName, varName, pointToString(startLogical), pointToString(endLogical), stopsText);
}

/*!
 * \brief GradientGenerator::generate - the private method that actually generates the gradient
 * based on \param config.in values.
 * It traverses the \param config.in.image pixel by pixel in \param config.in.direction with \param config.in.step
 * to set up the appropriate values of target QLinearGradient.
 * \return true if the generation process succeeds.
 * The result is stored in \param config.out fields.
 */
/*static*/ bool GradientGenerator::generate(LinGradGen &config)
{
    QPainterPath path;
    path.moveTo(config.in.direction.p1());
    path.lineTo(config.in.direction.p2());

    const QPointF unitV = config.in.direction.unitVector().translated(-config.in.direction.unitVector().p1()).p2();
    const QPointF shift = unitV * (config.in.direction.length() / qreal(config.in.step));

    QPointF pnt(config.in.direction.p1());
    QLineF progressLine(pnt, pnt);
    config.out.gradientStops.clear();
    QString cppString;
    while (progressLine.length() < config.in.direction.length()) {
        progressLine.setP2(pnt);
        if (config.in.image.rect().contains(pnt.toPoint())) {
            const QColor color = config.in.image.pixelColor(pnt.toPoint());
            const qreal posInGradient = path.percentAtLength(progressLine.length());
            config.out.gradientStops.append({ posInGradient, color });

            cppString += QString("        { %1, QColor::fromRgb( %2, %3, %4) },\n")
                                 .arg(QString::number(posInGradient), QString::number(color.red()),
                                      QString::number(color.green()), QString::number(color.blue()));
        }
        pnt += shift;
    }

    const QLineF unit = config.in.direction.translated(-config.in.direction.p1()).unitVector();
    QPointF start(unit.p1()), stop(unit.p2());
    for (qreal *c : { &stop.rx(), &stop.ry() })
        if (*c < 0)
            *c *= -1;

    fillResult(config, GeneratorResult::Error::NoError, QString());
    config.out.cppCode = prepareText(config.in.varName, start, stop, cppString);
    config.out.direction = { start, stop };

    return !config.out.failed();
}
