/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#include "mainwindow.h"

#include "gradientgenerator.h"
#include "gradientpreview.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QFile>
#include <QFileDialog>
#include <QPainter>
#include <QScrollBar>

QGradientStops createTestGradient()
{
    QVector<QColor> colors;
    for (int i = 0; i <= 300; i += 60)
        colors << QColor::fromHsv(i, 255, 255);
    colors << colors.first();

    colors.takeFirst();
    //        colors.takeLast();

    const qreal onePercent = 100. / (colors.size() - 1);

    QGradientStops result;
    for (int i = 0; i < colors.size(); ++i)
        result << QGradientStop({ (onePercent * i) / 100., colors.at(i) });

    return result;
}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , previewIn(new GradientPreview(this))
    , previewOut(new GradientPreview(this))
{
    ui->setupUi(this);

    ui->saPreviewIn->setWidget(previewIn);

    connect(previewIn, &GradientPreview::mouseMoved, this, &MainWindow::onPreviewMouseMoved);
    connect(previewIn, &GradientPreview::requestGenerate, this, &MainWindow::onGenerateRequested);

    ui->saPreviewOut->setWidget(previewOut);
    connect(previewOut, &GradientPreview::mouseMoved, this, &MainWindow::onPreviewMouseMoved);

    for (auto splitter : { ui->splitterIn, ui->splitterOut })
        splitter->setSizes(QList<int>({ INT_MAX, INT_MAX }));

    connect(ui->saPreviewIn->verticalScrollBar(), &QScrollBar::valueChanged, ui->saPreviewOut->verticalScrollBar(),
            &QScrollBar::setValue);
    connect(ui->saPreviewOut->verticalScrollBar(), &QScrollBar::valueChanged, ui->saPreviewIn->verticalScrollBar(),
            &QScrollBar::setValue);

    connect(ui->saPreviewIn->horizontalScrollBar(), &QScrollBar::valueChanged, ui->saPreviewOut->horizontalScrollBar(),
            &QScrollBar::setValue);
    connect(ui->saPreviewOut->horizontalScrollBar(), &QScrollBar::valueChanged, ui->saPreviewIn->horizontalScrollBar(),
            &QScrollBar::setValue);

    connect(previewIn, &GradientPreview::directionChanged, ui->previewInConf, &GeneratorConfig::setDirection);
    connect(ui->previewInConf, &GeneratorConfig::configChanged, this, &MainWindow::onPreviewConfigChanged);

    //    QImage img(200, 400, QImage::Format_RGB32);
    //    QPainter p(&img);
    //    QLinearGradient lg(img.rect().bottomLeft(), img.rect().topLeft());
    //    lg.setStops(createTestGradient());
    //    p.fillRect(img.rect(), lg);
    //    img.save("./test4.png");
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onPreviewMouseMoved(const QString &info)
{
    statusBar()->showMessage(info);
}

void MainWindow::onGenerateRequested()
{
    GradientGenerator::LinGradGen params;
    params.in.path = ui->leImagePath->text();
    params.in.image = *previewIn->getImage();
    params.in.direction = previewIn->getDirection();
    params.in.step = ui->previewInConf->getStep();
    params.in.varName = ui->previewInConf->getVarName();

    const bool failed = !GradientGenerator::generateCode(params);
    ui->resultDisplay->setPlainText(failed ? params.out.statusDescription : params.out.cppCode);
    previewOut->setPreview(params.out.gradientStops, params.out.direction, params.in.image.size());
}

void MainWindow::onPreviewConfigChanged()
{
    previewIn->setDirection(ui->previewInConf->getDirection());
}

QStringList supportedImages()
{
    return {};
}

void MainWindow::on_btnOpenFile_clicked()
{
    const QString path =
            QFileDialog::getOpenFileName(this, tr("Select image"), currentFile, supportedImages().join('\n'));
    openFile(path);
}

void MainWindow::openFile(const QString &filePath)
{
    if (filePath.isEmpty())
        return;

    QFile imgFile(filePath);
    if (!imgFile.exists())
        return;

    if (!imgFile.open(QIODevice::ReadOnly)) {
        qDebug() << "Can't open file" << filePath << imgFile.errorString();
        return;
    }

    imgFile.close();

    QImage img(filePath);
    if (img.isNull())
        return;

    currentFile = filePath;
    ui->leImagePath->setText(currentFile);
    previewIn->setPreview(currentFile);
}
