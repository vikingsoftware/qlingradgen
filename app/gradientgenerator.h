/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#pragma once

#include <QLineF>
#include <QLinearGradient>
#include <QObject>

class QImage;
/*!
 * \brief The GradientGenerator class is the actual worker used to generate the C++ code.
 */
class GradientGenerator : public QObject
{
    Q_OBJECT
public:
    /*!
     * \brief The GeneratorInput struct - handler of input data to be used in generation process
     */
    struct GeneratorInput {
        QString path;
        QImage image;
        QLineF direction;
        int step;
        QString varName;
    };

    /*!
     * \brief The GeneratorError struct - error description handler
     */
    struct GeneratorResult {
        /*!
         * \brief The Error enum
         * \value NoError
         *      No error occured.
         * \value NoInputFile
         *      A file does not exist or can not be opened for reading.
         * \value InvalidImage
         *      An image can not be loaded.
         * \value InvalidDirection
         *      The specificied direction line exceeds the image rectangle.
         * \value InvalidStep
         *      The specificied step is either <= 0 or exceeds the direction line lenght.
         */
        enum class Error
        {
            NoError = 0,
            NoInputFile,
            InvalidImage,
            InvalidDirection,
            InvalidStep,
            NoOutputName,
        };

        Error status { Error::NoError };
        QString statusDescription {};
        QString cppCode {};
        QGradientStops gradientStops;
        QLineF direction;

        bool failed() const { return GradientGenerator::GeneratorResult::Error::NoError != status; }
    };

    /*!
     * \brief The LinGradGen struct is used to handle input parameters and generation result;
     */
    struct LinGradGen {
        GeneratorInput in;
        GeneratorResult out;
    };

    static bool generateCode(LinGradGen &config);

private:
    explicit GradientGenerator(QObject *parent = nullptr) = delete;

    static bool fillResult(LinGradGen &param, GeneratorResult::Error code, const QString &description);
    static bool generate(LinGradGen &config);
};
