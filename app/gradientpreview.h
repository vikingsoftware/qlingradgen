/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#pragma once

#include <QWidget>
#include <memory>

class GradientPreview : public QWidget
{
    Q_OBJECT
public:
    explicit GradientPreview(QWidget *parent = nullptr);
    ~GradientPreview();

    void setPreview(const QString &imagePath);
    void setPreview(const QGradientStops &stops, const QLineF &direction, const QSize &size);

    QImage *getImage() const;
    QLineF getDirection() const;
    void setDirection(const QLineF &line);

    QSize sizeHint() const override;
signals:
    void mouseMoved(const QString &info) const;
    void requestGenerate() const;
    void directionChanged(const QLineF &line, const QRect &imgRect) const;

protected:
    void paintEvent(QPaintEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

    bool event(QEvent *event) override;

private:
    struct GradientPreviewPrivate;
    const std::unique_ptr<GradientPreview::GradientPreviewPrivate> d;
    void updateSelector();

    QPoint adjustPointToImage(const QPoint &pnt) const;
    void notifyCurrentColor(const QPoint &currPos, bool showCtrlHint) const;
};
