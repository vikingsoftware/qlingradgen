/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#pragma once

#include <QWidget>

namespace Ui {
class GeneratorConfig;
}

class GeneratorConfig : public QWidget
{
    Q_OBJECT

public:
    explicit GeneratorConfig(QWidget *parent = nullptr);
    ~GeneratorConfig();

    int getStep() const;
    QString getVarName() const;

    QLineF getDirection() const;

public slots:
    void setDirection(const QLineF &direction, const QRect &maxSize);

private slots:
    void notifyConfigChanged();
    void on_btnInvert_clicked();
    void on_btnRotate_clicked();

signals:
    void configChanged() const;

private:
    Ui::GeneratorConfig *ui;
};
