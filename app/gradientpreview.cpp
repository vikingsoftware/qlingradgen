/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#include "gradientpreview.h"

#include <QDebug>
#include <QImage>
#include <QMouseEvent>
#include <QPainter>
#include <QPen>
#include <QToolTip>
#include <QtMath>

static const int sMargin = 10;

struct GradientPreview::GradientPreviewPrivate {
    GradientPreviewPrivate()
    {
        startSign.addEllipse(15, 15, 15, 15);
        endSign.addEllipse(20, 20, 20, 20);
    }
    virtual ~GradientPreviewPrivate() { delete image; }

    enum class Mode
    {
        Interactive = 0,
        Static
    };

    bool previewOnly() const { return mode == Mode::Static; }

    void setupImage(QImage *img)
    {
        if (image)
            delete image;
        image = img;

        if (image) {
            imageRect = image->rect();
            imageGeometry = imageRect.translated(sMargin, sMargin);
            viewport = imageGeometry.adjusted(-sMargin, -sMargin, sMargin, sMargin);
        }
    }

    Mode mode { Mode::Static };
    QImage *image { nullptr };
    QRect imageRect, imageGeometry, viewport;
    QPainterPath startSign, endSign, selector;
    QPainterPath *activeEdge { nullptr };
};

GradientPreview::GradientPreview(QWidget *parent)
    : QWidget(parent)
    , d(new GradientPreviewPrivate())
{
    setMouseTracking(true);
}

GradientPreview::~GradientPreview() = default;

void GradientPreview::setPreview(const QString &imagePath)
{
    d->mode = GradientPreviewPrivate::Mode::Interactive;
    d->setupImage(new QImage(imagePath));
    setFixedSize(d->viewport.size());

    QPointF start, end;
    const QPointF center = d->imageGeometry.center();
    const bool isHorizontal = d->imageRect.width() >= d->imageRect.height();
    if (isHorizontal) {
        start = QPointF(d->imageGeometry.left(), center.y());
        end = QPointF(d->imageGeometry.right(), center.y());
    } else {
        start = QPointF(center.x(), d->imageGeometry.top());
        end = QPointF(center.x(), d->imageGeometry.bottom());
    }

    d->startSign.translate(start - d->startSign.boundingRect().center());
    d->endSign.translate(end - d->endSign.boundingRect().center());
    updateSelector();

    emit requestGenerate();
}

void GradientPreview::setPreview(const QGradientStops &stops, const QLineF &direction, const QSize &size)
{
    QLinearGradient lg;
    lg.setCoordinateMode(QGradient::ObjectMode);
    lg.setStops(stops);
    lg.setStart(direction.p1());
    lg.setFinalStop(direction.p2());
    d->mode = GradientPreviewPrivate::Mode::Static;
    d->setupImage(new QImage(size, QImage::Format_RGB32));
    setFixedSize(d->viewport.size());

    d->image->fill(Qt::transparent);
    QPainter p(d->image);
    p.fillRect(d->imageRect, lg);

    updateSelector();
}

QImage *GradientPreview::getImage() const
{
    return d->image;
}

QLineF GradientPreview::getDirection() const
{
    const QLineF line = { d->startSign.boundingRect().center().toPoint(),
                          d->endSign.boundingRect().center().toPoint() };
    return line.translated(-sMargin, -sMargin);
}

void GradientPreview::setDirection(const QLineF &line)
{
    {
        QSignalBlocker sb(this);
        auto calcShift = [](const QPainterPath &grip, const QPointF &destination) {
            return destination - grip.boundingRect().center();
        };

        const QLineF translated(line.translated(sMargin, sMargin));
        d->startSign.translate(calcShift(d->startSign, translated.p1()));
        d->endSign.translate(calcShift(d->endSign, translated.p2()));
        updateSelector();
    }

    emit requestGenerate();
}

QSize GradientPreview::sizeHint() const
{
    if (d->image)
        return d->image->size();
    return QWidget::sizeHint();
}

void GradientPreview::paintEvent(QPaintEvent *event)
{
    QWidget::paintEvent(event);

    QPainter painter(this);
    painter.fillRect(d->viewport, Qt::white);

    if (d->image) {
        painter.drawImage(d->imageGeometry, *d->image, d->imageRect);

        if (d->previewOnly())
            return;

        painter.setRenderHint(QPainter::Antialiasing);

        QPen marker;
        marker.setColor(Qt::white);
        marker.setWidthF(2);

        painter.setPen(marker);
        painter.drawPath(d->selector);

        marker.setColor(Qt::black);
        marker.setWidthF(1);
        painter.strokePath(d->selector, marker);

        if (d->activeEdge) {
            marker.setColor(Qt::yellow);
            marker.setWidthF(1);
            painter.strokePath(*d->activeEdge, marker);
        }
    }
}

void GradientPreview::mousePressEvent(QMouseEvent *event)
{
    QWidget::mousePressEvent(event);

    if (d->previewOnly())
        return;

    if (event->buttons() == Qt::LeftButton) {
        const QPoint currMouse = event->pos();
        if (d->startSign.boundingRect().contains(currMouse)) {
            d->activeEdge = &d->startSign;
        } else if (d->endSign.boundingRect().contains(currMouse)) {
            d->activeEdge = &d->endSign;
        } else {
            d->activeEdge = nullptr;
        }
        updateSelector();
    }
}

void GradientPreview::mouseMoveEvent(QMouseEvent *event)
{
    QWidget::mouseMoveEvent(event);

    bool showCtrlHint(false);
    if (!d->previewOnly()) {
        QPoint currMouse = adjustPointToImage(event->pos());
        if (event->buttons() == Qt::LeftButton) {
            if (d->activeEdge) {
                showCtrlHint = event->modifiers() != Qt::ControlModifier;
                if (!showCtrlHint) {
                    const QPainterPath *other = d->activeEdge == &d->startSign ? &d->endSign : &d->startSign;
                    const QPointF otherCenter = other->boundingRect().center();
                    QLineF line(otherCenter, currMouse);
                    const qreal newAngle = qCeil(line.angle() / 15) * 15;
                    line.setAngle(newAngle);
                    currMouse = adjustPointToImage(line.p2().toPoint());
                }

                const QPointF currCenter = d->activeEdge->boundingRect().center();
                const QPointF delta = currMouse - currCenter;
                d->activeEdge->translate(delta);
                updateSelector();
            }
        }
    }

    if (d->image) {
        const QPoint pixel = d->activeEdge ? adjustPointToImage(d->activeEdge->boundingRect().center().toPoint())
                                           : adjustPointToImage(event->pos());
        notifyCurrentColor(pixel - QPoint(sMargin, sMargin), showCtrlHint);
    }
}

void GradientPreview::mouseReleaseEvent(QMouseEvent *event)
{
    QWidget::mouseReleaseEvent(event);

    if (d->previewOnly())
        return;

    emit requestGenerate();
}

void GradientPreview::updateSelector()
{
    d->selector = QPainterPath();
    d->selector.addPath(d->startSign);
    d->selector.addPath(d->endSign);

    d->selector.moveTo(d->startSign.boundingRect().center());
    d->selector.lineTo(d->endSign.boundingRect().center());

    update();

    emit directionChanged(getDirection(), d->imageRect);
}

void GradientPreview::notifyCurrentColor(const QPoint &currPos, bool showCtrlHint) const
{
    if (!d->image)
        return;

    static const QString message("%1x%2: [%3,%4,%5]%6");
    const QString ctrlHint = showCtrlHint ? tr(" Hint: Press CTRL to straighten the line.") : QString();
    const QColor c = d->image->pixelColor(currPos);

    emit mouseMoved(message.arg(currPos.x()).arg(currPos.y()).arg(c.red()).arg(c.green()).arg(c.blue()).arg(ctrlHint));
}

QPoint GradientPreview::adjustPointToImage(const QPoint &pnt) const
{
    return { qBound(d->imageGeometry.left(), pnt.x(), d->imageGeometry.right()),
             qBound(d->imageGeometry.top(), pnt.y(), d->imageGeometry.bottom()) };
}

bool GradientPreview::event(QEvent *event)
{
    switch (event->type()) {
    case QEvent::ToolTip: {
        QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
        const QPoint currPos = helpEvent->pos();
        QString msg;
        if (d->startSign.boundingRect().contains(currPos))
            msg = tr("Start point");
        else if (d->endSign.boundingRect().contains(currPos))
            msg = tr("End point");
        else
            msg = d->previewOnly() ? tr("Generated gradient") : tr("Gradient image");

        if (!msg.isEmpty()) {
            QToolTip::showText(helpEvent->globalPos(), msg);
            return false;
        }
        break;
    }
    default:
        break;
    }

    return QWidget::event(event);
}
