/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#pragma once
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui {
class MainWindow;
}
QT_END_NAMESPACE

class GradientPreview;
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void onPreviewMouseMoved(const QString &info);
    void onGenerateRequested();
    void onPreviewConfigChanged();
    void on_btnOpenFile_clicked();

private:
    Ui::MainWindow *ui { nullptr };
    GradientPreview *previewIn { nullptr };
    GradientPreview *previewOut { nullptr };
    QString currentFile;

    void openFile(const QString &filePath);
};
