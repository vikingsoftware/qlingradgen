/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#include "generatorconfig.h"

#include "ui_generatorconfig.h"

GeneratorConfig::GeneratorConfig(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::GeneratorConfig)
{
    ui->setupUi(this);

    for (auto sb : { ui->sbX1, ui->sbX2, ui->sbY1, ui->sbY2, ui->sbStep })
        connect(sb, QOverload<int>::of(&QSpinBox::valueChanged), this, &GeneratorConfig::notifyConfigChanged);

    connect(ui->lineEdit, &QLineEdit::editingFinished, this, &GeneratorConfig::notifyConfigChanged);
}

GeneratorConfig::~GeneratorConfig()
{
    delete ui;
}

void GeneratorConfig::setDirection(const QLineF &direction, const QRect &maxSize)
{
    QSignalBlocker sb(this);
    for (auto sb : { ui->sbX1, ui->sbX2 })
        sb->setRange(maxSize.left(), maxSize.right());
    for (auto sb : { ui->sbY1, ui->sbY2 })
        sb->setRange(maxSize.top(), maxSize.bottom());

    ui->sbX1->setValue(direction.x1());
    ui->sbY1->setValue(direction.y1());
    ui->sbX2->setValue(direction.x2());
    ui->sbY2->setValue(direction.y2());

    notifyConfigChanged(); // to actualize step and its range
}

QLineF GeneratorConfig::getDirection() const
{
    const QLineF line(ui->sbX1->value(), ui->sbY1->value(), ui->sbX2->value(), ui->sbY2->value());
    return line;
}

void GeneratorConfig::notifyConfigChanged()
{
    ui->sbStep->setRange(2, getDirection().length());
    emit configChanged();
}

int GeneratorConfig::getStep() const
{
    return ui->sbStep->value();
}

QString GeneratorConfig::getVarName() const
{
    const QString text = ui->lineEdit->text();
    return text.isEmpty() ? ui->lineEdit->placeholderText() : text;
}

void GeneratorConfig::on_btnInvert_clicked()
{
    const QLineF &line = getDirection();

    QPointF start = line.p1();
    QPointF end = line.p2();

    qSwap(start, end);
    setDirection({ start, end },
                 { ui->sbX1->minimum(), ui->sbY1->minimum(), ui->sbX1->maximum(), ui->sbY1->maximum() });
    notifyConfigChanged();
}

void GeneratorConfig::on_btnRotate_clicked()
{
    const QLineF &line = getDirection();
    const QRectF r(ui->sbX1->minimum(), ui->sbY1->minimum(), ui->sbX1->maximum(), ui->sbY1->maximum());
    const QLineF top(r.topLeft(), r.topRight());
    const QLineF right(r.topRight(), r.bottomRight());

    QLineF norm = line.normalVector();

    QPointF intersection;
    if (norm.intersects(top, &intersection) != QLineF::IntersectType::NoIntersection) {
        norm.setP1(intersection);
        norm.setP2({ intersection.x(), r.bottom() });
    } else if (norm.intersects(right, &intersection) != QLineF::IntersectType::NoIntersection) {
        norm.setP1({ r.left(), intersection.y() });
        norm.setP2(intersection);
    }
    norm.translate(r.center() - norm.center());

    setDirection(norm, r.toRect());
    notifyConfigChanged();
}
