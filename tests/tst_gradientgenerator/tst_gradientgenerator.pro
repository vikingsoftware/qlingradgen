QT += testlib widgets

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle

TEMPLATE = app

APP_SRC_ROOT=../../app

INCLUDEPATH += \
    $$APP_SRC_ROOT

HEADERS += \
    $$APP_SRC_ROOT/gradientgenerator.h

SOURCES += \
    $$APP_SRC_ROOT/gradientgenerator.cpp \
    tst_gradientgenerator.cpp

RESOURCES += \
    ../../app/resources/rsc.qrc
