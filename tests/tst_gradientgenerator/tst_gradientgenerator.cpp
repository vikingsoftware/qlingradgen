/*
   Copyright (C) 2020-21 Denis Gofman - <denis@vikingsoftware.com>

   This library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public
   License as published by the Free Software Foundation; either
   version 2 of the License, or (at your option) any later version.

   This library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public License
   along with this program. If not, see <https://www.gnu.org/licenses/lgpl-2.1.html>.
*/

#include "gradientgenerator.h"

#include <QBoxLayout>
#include <QPainter>
#include <QtTest>

namespace TestHelper {
static constexpr qreal sStep = 100.;

bool compareColors(const QColor &c1, const QColor &c2, int tolerance)
{
    auto check = [&tolerance](int a, int b) { return qAbs(a - b) <= tolerance; };

    const QVector<int> rgb1 { c1.red(), c1.green(), c1.blue() };
    const QVector<int> rgb2 { c2.red(), c2.green(), c2.blue() };

    for (int i = 0; i < 3; ++i)
        if (!check(rgb1.at(i), rgb2.at(i))) {
            qDebug() << rgb1;
            qDebug() << rgb2;
            return false;
        }

    return true;
}

void compareImages(const QImage &imgIn, const QImage &imgOut, QBoxLayout::Direction direction)
{
    static constexpr int tolerance = 6.;
    const qreal halfHeight = imgIn.height() / 2.;
    const qreal halfWidth = imgIn.width() / 2.;

    const bool isHorizontal = QBoxLayout::LeftToRight == direction || QBoxLayout::RightToLeft == direction;
    if (isHorizontal) {
        for (int i = 0; i < halfWidth; ++i)
            for (auto pixel : { QPoint(i, halfHeight), QPoint(halfWidth + i, halfHeight) })
                QVERIFY(compareColors(imgIn.pixelColor(pixel), imgOut.pixelColor(pixel), tolerance));
    } else {
        for (int i = 0; i < halfHeight; ++i)
            for (auto pixel : { QPoint(halfWidth, i), QPoint(halfWidth, halfHeight + i) })
                QVERIFY(compareColors(imgIn.pixelColor(pixel), imgOut.pixelColor(pixel), tolerance));
    }
}

QImage outImage(const QImage &in, const QGradientStops &stops, const QLineF &imgDir, const QLineF &lgDir)
{
    QLinearGradient lg(imgDir.p1(), imgDir.p2());
    lg.setCoordinateMode(QGradient::ObjectMode);
    lg.setStops(stops);
    lg.setStart(lgDir.p1());
    lg.setFinalStop(lgDir.p2());

    QImage imgOut(in.size(), in.format());
    QPainter p(&imgOut);
    p.fillRect(imgOut.rect(), lg);
    return imgOut;
}

const GradientGenerator::LinGradGen invokeGenerator(const QString &path, const QLineF &direction, int steps)
{
    GradientGenerator::LinGradGen params;
    params.in.path = path;
    params.in.direction = direction;
    params.in.step = steps;
    params.in.varName = QLatin1String("lg");
    auto checkresult = [](auto actual, auto expected) { QCOMPARE(actual, expected); };

    const bool generated = GradientGenerator::generateCode(params);
    checkresult(generated, true);
    checkresult(params.out.statusDescription, QString());
    checkresult(params.out.status, GradientGenerator::GeneratorResult::Error::NoError);

    return params;
}

QLineF createDirection(const QImage &img, QBoxLayout::Direction direction)
{
    const qreal w = img.width();
    const qreal h = img.height();
    const qreal halfH = h / 2.;
    const qreal halfW = w / 2.;
    auto pixel = [&w, &h](qreal x, qreal y) { return QPointF(qBound(0., x, w - 1.), qBound(0., y, h - 1.)); };

    switch (direction) {
    case QBoxLayout::LeftToRight:
        return { pixel(0., halfH), pixel(w - 1., halfH) };
    case QBoxLayout::RightToLeft:
        return { pixel(w, halfH), pixel(0, halfH) };
    case QBoxLayout::TopToBottom:
        return { pixel(halfW, 0.), pixel(halfW, h - 1.) };
    case QBoxLayout::BottomToTop:
        return { pixel(halfW, h - 1.), pixel(halfW, 0.) };
    }

    Q_UNREACHABLE();
    return {};
}

} // ns TestHelper

class GradientGeneratorTest : public QObject
{
    Q_OBJECT

    static const QMap<int, QString> sTestData;
    void performTest(QBoxLayout::Direction dir);

private slots:
    void test_leftToRight();
    void test_rightToLeft();
    void test_topToBottom();
    void test_bottomToTop();
    void test_fails();
};

/*static*/ const QMap<int, QString> GradientGeneratorTest::sTestData = {
    { QBoxLayout::LeftToRight, QStringLiteral(":/defaults/test1.png") },
    { QBoxLayout::RightToLeft, QStringLiteral(":/defaults/test2.png") },
    { QBoxLayout::TopToBottom, QStringLiteral(":/defaults/test3.png") },
    { QBoxLayout::BottomToTop, QStringLiteral(":/defaults/test4.png") },
};

void GradientGeneratorTest::performTest(QBoxLayout::Direction dir)
{
    const QString data = sTestData[dir];
    const QImage imgIn(data);

    int steps;
    bool reflectHor(false), reflectVer(false);
    switch (dir) {
    case QBoxLayout::LeftToRight:
        steps = imgIn.width();
        break;
    case QBoxLayout::RightToLeft:
        steps = imgIn.width();
        reflectHor = true;
        break;
    case QBoxLayout::TopToBottom:
        steps = imgIn.height();
        break;
    case QBoxLayout::BottomToTop:
        steps = imgIn.height();
        reflectVer = true;
        break;
    default:
        break;
    }

    const GradientGenerator::LinGradGen &res =
            TestHelper::invokeGenerator(data, TestHelper::createDirection(imgIn, dir), steps);

    const QImage &imgOut = TestHelper::outImage(imgIn, res.out.gradientStops, res.in.direction, res.out.direction);

    const QImage &prepared = (reflectVer || reflectHor) ? imgOut.mirrored(reflectHor, reflectVer) : imgOut;

    //    imgIn.save(QString("./tst_%1_in.png").arg(dir));
    //    imgOut.save(QString("./tst_%1_out.png").arg(dir));
    //    prepared.save(QString("./tst_%1_prepared.png").arg(dir));

    TestHelper::compareImages(imgIn, prepared, dir);
}

void GradientGeneratorTest::test_leftToRight()
{
    performTest(QBoxLayout::LeftToRight);
}

void GradientGeneratorTest::test_rightToLeft()
{
    performTest(QBoxLayout::RightToLeft);
}

void GradientGeneratorTest::test_topToBottom()
{
    performTest(QBoxLayout::TopToBottom);
}

void GradientGeneratorTest::test_bottomToTop()
{
    performTest(QBoxLayout::BottomToTop);
}

void GradientGeneratorTest::test_fails()
{
    GradientGenerator::LinGradGen params;

    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::NoInputFile);

    params.in.path = "/no/such/a.file";
    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::NoInputFile);

    params.in.path = ":/defaults/test5.png";
    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::InvalidImage);

    params.in.path = ":/defaults/test1.png";
    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::InvalidDirection);

    params.in.direction = { -100., -100., -1., -1. };
    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::InvalidStep);

    params.in.step = 1;
    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::InvalidDirection);

    const QPointF shift(1., 1.);
    params.in.direction = { params.in.image.rect().topLeft() - shift, params.in.image.rect().bottomRight() + shift };
    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::InvalidDirection);

    params.in.direction = { params.in.image.rect().topLeft(), params.in.image.rect().topRight() };
    params.in.step = params.in.image.width() + 1;
    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::InvalidStep);

    params.in.step = 1;
    QVERIFY(GradientGenerator::generateCode(params) == false);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::NoOutputName);

    params.in.varName = QStringLiteral("testGradient");
    QVERIFY(GradientGenerator::generateCode(params) == true);
    QCOMPARE(params.out.status, GradientGenerator::GeneratorResult::Error::NoError);
}

QTEST_APPLESS_MAIN(GradientGeneratorTest)

#include "tst_gradientgenerator.moc"
