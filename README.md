## What

***QLinGradGen*** — [`QLinearGradient`](https://doc.qt.io/qt-5/qlineargradient.html) Generator, a tool to generate C++ code with a [`QLinearGradient`](https://doc.qt.io/qt-5/qlineargradient.html) instance initialization based on the values from a raster image.

## Why

Sometimes, a task for a developer sounds like "Use the *Jet* gradient here" or "Put the same gradient like the one used in the prev application version (we have no sources of)" or even just "find something on the internet", so all you have to deal with is just a raster image.

## How

This tool scans such image pixels and generates the code to initialize an instance of [`QLinearGradient`](https://doc.qt.io/qt-5/qlineargradient.html) to be pasted into your code or used as a base for a color map in a non-Qt-based project.

<img src="readme_img/sample_0.png" width="1200" height="816"/>